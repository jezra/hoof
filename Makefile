heybuddy:
	@clear && echo "hey buddy!"
	# Make Options:
	# compile: compile src/hoof-server 
	# // compile-dist: compile and create/fill a dist directory for distribution
	# serve-dev: run the src/hoof.server application
	
compile:
	@cd src && go build -ldflags=-w -o hoof.server hoof.go
	
compile-dist:
	# cd src and compile
	# remove dist dir
	# make dist dir
	# cp server to dist
	# cp templates to dist
	# cp/make config?
	
serve-dev:
	@cd src && ./hoof.server 1