# Hello

The purpose of this project is to create a very basic code snippet sharing webapp. By "very basic", I mean as simple/complex
as I feel the project needs to be. Yea, that's hella vague. I am User Zero, and there are needs to be met.

* have fun
* write code
* make a useful tool

There is joy in making tools :)

## Usage
### Compiling
Hoof is written in Go and is compiled with 
`make compile`
this will create `hoof.server` in the src directory

### Testing locally
mv the `config.json.EXAMPLE` file to `src/config.json` and edit as you wish

Compiled hoof.server can be run by 
`make serve-dev`