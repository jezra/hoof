/* Hoof 
copyright 2023 Jezra
released under GPLv3
*/

/* global fetch */
/* global document */
/* global alert */
class HoofInterface {
	constructor() {}
	init() {
		// create reference to self, because I can't trust "this" /me has trust issues
		var self = this;
		self.newCodeString = "Add New Code";
		self._id('toggleInputs').textContent = self.newCodeString;
		self.toggledShow = false;
		/* set up buttons and forms */
		// perform actions
		self._id('toggleInputs').onclick = function (event) {
			self.clickedToggle(self, event);
		};
		self._id('addSnippet').onsubmit = function (event) {
			self.addSnippetSubmit(self, event);
		};
		//what are we looking at?
		self.requestPath = document.location.pathname.substring(1);
		if (self.requestPath) {
			// hide the add snippet
			self._id('add-snippet').setAttribute("class", "hidden");
			//self.fetchCode(self);
		}
		else {
			//self.fetchCodes(self);
		}
	}
	// make find by id less verbose
	_id(value) {
		return document.getElementById(value);
	}
	// make find by class less verbose
	_class(value) {
		return document.getElementsByClassName(value);
	}
	addSnippetSubmit(self, event) {
		//stop default
		event.preventDefault();
		//collect some variables
		var description = event.target.snippetDescription.value;
		var language = event.target.snippetLanguage.value;
		var code = event.target.snippetCode.value;
		var auth = event.target.auth.value;
		let payload = { "description": description, "language": language, "code": code, "auth": auth };
		fetch("/codes/", {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(payload)
		}).then((response) => {
			if (!response.ok) {
				throw new Error('Network response was not ok');
			}
			// process the response
			response.json().then((json) => {
				console.log(json);
				if (json.error) {
					alert(json.error);
				}
				else {
					// response will be {"file": VALUE}
					// redirect to /VALUE
					document.location.href = "/" + json.file;
				}
			});
		});
	}
	clickedToggle(self, event) {
		if (self.toggledShow) {
			// hide the div
			self._id("create").setAttribute("class", "hidden");
			// change the text
			console.log(self);
			self._id('toggleInputs').textContent = self.newCodeString;
			// set the toggleShow
			self.toggledShow = false;
		}
		else {
			// show the div
			self._id("create").setAttribute("class", "");
			// change the text
			self._id('toggleInputs').textContent = "Hide Form"
			// set the toggleShow
			self.toggledShow = true;
		}
	}
	processCodes(self, data) {
		let Html = "";
		let content = self._id('content');
		if (data) {
			if (data.length == 0) {
				Html = "There are no code examples, bummer.";
			}
			else {
				Html = "<ul>";
				data.snippets.forEach((snippet) => {
					Html += "<li><a href='/" + snippet + "'>" + snippet + "</a></li>";
				});
				Html += "</ul>";
			}
		}
		content.innerHTML = Html;
	}
	/* API requests */
	fetchCode(self) {
		let endpoint = `/codes/${self.requestPath}`;
		console.log(endpoint);
		fetch(endpoint, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			},
		}).then((response) => {
			if (!response.ok) {
				throw new Error('Network response was not ok');
			}
			// process the response
			response.json().then((json) => {
				// add the description
				let descriptionDiv = document.createElement("div");
				descriptionDiv.textContent = json.description;
				self._id('content').appendChild(descriptionDiv);
				// add the code
				let preDiv = document.createElement("pre");
				let codeDiv = document.createElement("code");
				if (json.language) {
					codeDiv.setAttribute("class", `language-${json.language}`);
				}
				preDiv.appendChild(codeDiv);
				codeDiv.textContent = json.code;
				self._id('content').appendChild(preDiv);
				// run the highlighter
				hljs.highlightAll();
			});
		});
	}
	fetchCodes(self) {
		fetch('/codes', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			},
		}).then((response) => {
			if (!response.ok) {
				throw new Error('Network response was not ok');
			}
			// process the response
			response.json().then((json) => {
				self.processCodes(self, json);
			});
		});
	}
}
let hi = new HoofInterface();
hi.init();
