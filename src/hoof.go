/* Hoof 
copyright 2023 Jezra
released under GPLv3
*/
package main
import (
	"fmt"
	"os"
	"net/http"
	"log"
	"path/filepath"
	"strings"
	"encoding/json"
	"time"
	"io"
	"io/ioutil"
	"html"
)
	
type snippetStruct struct{
	Language string
	Code string
	Description string
	Auth string
}
type Config struct {
	Port int32
	Auth string
}
var verbose bool
var publicDir string
var snippetsDir string
var templateDir string

var config Config

func validCodeFile(codeFile string) (bool){
	// is the codeFile in the codeFileDir?
	codeSnippets := getSnippetNames()
	inArray := false
	for i := range codeSnippets {
		if codeFile == codeSnippets[i] {
			inArray = true
			break
		}		
	}
	return inArray
}

// get the codeSnippets from the snippets Directory as an array
func getSnippetNames() []string {
	// local array of file paths
	files := []string{}
	err := filepath.Walk(snippetsDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Println(err)
			return err
		}
		isDir := info.IsDir()
		name := filepath.Base(path)
		if !isDir && !strings.HasPrefix(name, ".") {
			files = append(files, name)
		}
		return nil
	})
	if err != nil {
			fmt.Println(err)
	}
	return files
}

func fetchHTML(snippetPath string) string {
	var responseHtml string
	// read index.html into a string
	bytes, err := ioutil.ReadFile(templateDir+"/index.html")
	if err != nil {
		log.Fatal("index.html: %s", err)
	}
	indexHTML := string(bytes)
	if (snippetPath == ""){// empty snippetPath? index
		// get the snippets
		snippets := getSnippetNames()
		// convert snippets to html links
		list := "<ul>"
		for _, snippet := range snippets {
			list+="<li><a href='/"+snippet+"'>"+snippet+"</a></li>"	 
		}
		list+="</ul>"
		// merge into the index
		responseHtml = strings.Replace(indexHTML, "[CONTENT]", list, 1)
	} else {
		// read snippet.html into a string
		bytes, err = ioutil.ReadFile(templateDir+"/snippet.html")
		if err != nil {
			log.Fatal("snippet.html: %s", err)
		}
		snippetHTML := string(bytes)
		// read snippet as a json object
		var snip snippetStruct
		
		// read all bytes from snippet file
		// read the config
		bytes, err = ioutil.ReadFile(snippetPath)
		if err != nil {
			log.Fatal("snippetPath: %s", err)
		}
			
		//decode 
		json.Unmarshal(bytes, &snip)
		// merge snippet into snippet.html
		snippetHTML = strings.Replace(snippetHTML, "[DESCRIPTION]", snip.Description, 1)
		snippetHTML = strings.Replace(snippetHTML, "[LANGUAGE]", snip.Language, 1)
		// html encode the snip.Code 
		htmlEncodedCode := html.EscapeString(snip.Code)
		snippetHTML = strings.Replace(snippetHTML, "[HTML-ENCODED-CODE]", htmlEncodedCode, 1)
		// merge snippet.html into index.html
		responseHtml = strings.Replace(indexHTML, "[CONTENT]", snippetHTML, 1)
	}
	
	return responseHtml
}

/* -- handlers for server requests -- */
func handleRoot(w http.ResponseWriter, r *http.Request) {
	responseStr := ""
	pathParts := strings.Split(r.URL.Path,"/")
	fmt.Println(pathParts)
	fmt.Println(len(pathParts))
	// there are 2 parts, the first is empty string, the last is empty string or snippetfile name
	if (pathParts[1]!=""){
		snippetFile := pathParts[1]
		if (validCodeFile(snippetFile)){
			filePath := filepath.Join(snippetsDir , snippetFile)
			output:= fetchHTML(filePath)
			if (verbose){
				fmt.Println("path",filePath)
			}
			fmt.Fprint(w, output)
		} else {
			w.WriteHeader(404)
		}
	} else {
		responseStr = fetchHTML("")
	}
	// get the merged index as a string
	
	w.Header().Add("Cache-Control", "no-cache")
	fmt.Fprint(w, responseStr)
}
func handleAssets(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	fullPath := filepath.Join(publicDir, path)
	// set the header to 
	w.Header().Add("Cache-Control", "no-cache")
	http.ServeFile(w,r,fullPath)
}
func handleCodeSnippets(w http.ResponseWriter, r *http.Request) {
	// TODO: handle Create Read Destroy verbs
	// TODO: require auth for Create/Destroy 
	// headers
	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("Cache-Control", "no-cache")
	switch r.Method {
		
		case "PUT": 
			var snip snippetStruct
			
			// read all bytes from the request body
			bytes, err := io.ReadAll(r.Body)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println(string(bytes))
			//decode 
			json.Unmarshal(bytes,&snip)
			
			// is the snip.Auth == config.Auth?
			if (snip.Auth == config.Auth){
				// is there  and code and desc?
				if(snip.Code!="" && snip.Description!=""){
					now := time.Now().Unix()
					nowStr := fmt.Sprintf("%v", now)
		
					// create the file 
					outputFile := filepath.Join(snippetsDir, nowStr)
					fHandle, err := os.Create(outputFile)
					if err != nil {
						fmt.Println(err)
					}
					// close the file with defer
					defer fHandle.Close()
		
					//write the bytes to the file
					fHandle.Write(bytes)
					
					// define some response data
					data := "{\"file\": "+nowStr+"}"
					fmt.Fprintf(w, data) // send data to client side
				}
			} else {
				w.WriteHeader(404)
			}
		default: 
			w.WriteHeader(404)
	}
}
/* the main */
func main() {
	// should we be verbose?
	verbose = (len(os.Args) > 1) // add any comandline arg for verbosity
	// get the apps directory
	appDir, dErr := filepath.Abs(filepath.Dir(os.Args[0]))
	if dErr != nil {
		log.Fatal(dErr)
	}
	// public dir
	publicDir = filepath.Join(appDir,"public")
	// codeSnippets dir
	snippetsDir = filepath.Join(appDir,"snippets")
	// template dir
	templateDir = filepath.Join(appDir,"templates")
	
	// read the config
	data, err := ioutil.ReadFile(appDir+"/config.json")
	if err != nil {
		log.Fatal("Config: %s", err)
	}
	//create a var to hold the json object
	// unmarshall it
	err = json.Unmarshal(data, &config)
	if err != nil {
		log.Fatal("Config: %s", err)
	}
	
	http.HandleFunc("/", handleRoot) // set root handler
	http.HandleFunc("/assets/", handleAssets) 
	http.HandleFunc("/codes/", handleCodeSnippets)
	
	if verbose {
		//inform the user of serving
		log.Printf("Application Directory: %v", appDir)
		log.Printf("codeSnippets dir: %v", snippetsDir)
		log.Printf("Public dir: %v", publicDir)
		log.Printf("Serving on port: %v", config.Port)
	}
	//inform the user of serving
	
	//start serving
	err = http.ListenAndServe(fmt.Sprintf(":%v",config.Port), nil) // set listen port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
